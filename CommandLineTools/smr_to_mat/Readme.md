# **"smr_to_mat"** CommandLineTool

### Description
Converts a .smr type file to a .mat type file

### Inputs
- **smr_file**: file to be converted (.smr)
- **mat_file_name**: preferred name of converted file (.mat)

### Outputs
- **converted_file**: .mat type converted file
