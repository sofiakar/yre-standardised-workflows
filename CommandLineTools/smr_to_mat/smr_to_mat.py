#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/smr_to_mat) has to be updated

import argparse
import neo
import numpy as np
from scipy.io import savemat

# function that reads signal from .smr file and stores it as a .mat file
def smr_to_mat(input_file, output_file):
    reader = neo.io.Spike2IO(filename=input_file, try_signal_grouping=False)
    Raw_Data = reader.read(lazy=False)[0]
    samp_freq = [float(Raw_Data.segments[0].analogsignals[0].sampling_rate)]
    samples_number = np.min([len(s.magnitude) for s in Raw_Data.segments[0].analogsignals])
    value = [s.magnitude.T[0][:samples_number] for s in Raw_Data.segments[0].analogsignals]
    signal = {'value': value, 'Fs': samp_freq}
    savemat(output_file, signal)
    print('Signal saved to', output_file)

parser = argparse.ArgumentParser()
parser.add_argument('input_file', help='file where the signal is stored (.smr)')
parser.add_argument('--output_file', default='signal.mat', help='name of converted file (.mat)')
args = parser.parse_args()

# convert file from .smr to .mat
smr_to_mat(args.input_file, args.output_file)
