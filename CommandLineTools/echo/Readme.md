# **"echo"** CommandLineTool

### Description
Takes a string as input, echoes it and stores the stdout in "output.txt" file

### Inputs
- **message**: string to echo

### Outputs
- **out**: new file containing the message string
