# **"psd_vis"** CommandLineTool

### Description
Plots the PSD (Power Spectral Density) for all selected channels of a signal in a single diagram

### Inputs
- **input_file**: input file where the signal PSD is stored (.json)
- **output_file_name**: preferred name for output plot (.png)
- **channels**: selected signal channels

### Outputs
- **plot**: produced plot
