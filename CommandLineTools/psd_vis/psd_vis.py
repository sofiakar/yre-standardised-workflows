#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/psd_vis) has to be updated

import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# function that plots the PSD for selected channels of a signal together
def visualization(input_file, channels, output_file):
    fig, ax = plt.subplots(figsize=(12,6))
    psd = pd.read_json(input_file)
    if not channels:
        channels = range(len(psd['f']))
    for ch in channels:
        ax.plot(psd.loc[psd['ch']==ch]['f'].values[0], np.log(psd.loc[psd['ch']==ch]['pxx'].values[0]), label='ch '+ str(ch))
    ax.set_xscale('log')
    ax.set_xlabel('Frequency [Hz]')
    ax.set_ylabel('log (pxx)')
    ax.legend()
    plt.savefig(output_file)

parser = argparse.ArgumentParser()
parser.add_argument('input_file', help='file where the signal PSD is stored (.json)')
parser.add_argument('--output_file', default='output.png', help='file where output plot will be stored')
parser.add_argument('--channels', nargs="+", type=int, metavar='ch', help='selected signal channels')
args = parser.parse_args()

# visualize PSD
visualization(args.input_file, args.channels, args.output_file)
