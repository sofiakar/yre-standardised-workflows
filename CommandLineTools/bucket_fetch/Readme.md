# **"bucket_fetch"** CommandLineTool

### Description
Fetches a file from a Collab bucket using the Data Proxy

### Inputs
- **bucket_id**: the id of the Collab bucket
- **object_name**: the name of a file (inside the selected bucket) to download
- **token**: user's token (clb_oauth.get_token()) for the Data Proxy

### Outputs
- **fetched_file**: the downloaded file
