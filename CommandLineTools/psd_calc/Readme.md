# **"psd_calc"** CommandLineTool

### Description
Calculates the PSD (Power Spectral Density) for every selected channel of a signal

### Inputs
- **input_file**: input file (.mat) containing the signal
- **output_file_name**: preferred name of output file (.json), where the calculated PSD will be stored
- **channels**: selected signal channels

### Outputs
- **output_file**: file containing the PSDs of the selected channels
