#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/psd_calc) has to be updated

import argparse
import pandas as pd
from scipy.io import loadmat
from scipy.signal import welch

# function that computes the PSD (Power Spectral Density) for every selected channel in the signal
def calculate_psd(input_file, channels, output_file):
    signal = loadmat(input_file)
    samp_freq = signal['Fs'][0]
    psd = []
    if not channels:
        channels = range(len(signal['value']))
    for i,ch in enumerate(channels):
        print('Performing analysis (' + str(i+1) + '/' + str(len(channels)) + ')')
        f, pxx = welch(x=signal['value'][ch,:], fs=samp_freq, nperseg=2*samp_freq, nfft=10*samp_freq)
        psd.append({'f': f, 'pxx':pxx, 'ch':ch})
    df_psd = pd.DataFrame.from_records(psd)
    df_psd.to_json(output_file, indent=3)
    print('Analysis completed, results saved to', output_file)

parser = argparse.ArgumentParser()
parser.add_argument('input_file', help='file where the signal is stored (.mat)')
parser.add_argument('--output_file', default='psd.json', help='name of output file, where the calculated PSD will be stored (.json)')
parser.add_argument('--channels', nargs="+", type=int, metavar='ch', help='selected signal channels')
args = parser.parse_args()

# calculate PSD
calculate_psd(args.input_file, args.channels, args.output_file)
