#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/psd_sig_vis) has to be updated

import argparse
import os
import shutil
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.io import loadmat

# function that plots (for each selected channel) the signal and the PSD together
def visualization(sig_file, psd_file, channels, output_dir):
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(output_dir)
    sig = loadmat(sig_file)
    psd = pd.read_json(psd_file)
    if not channels:
        channels = range(len(psd['f']))
    for ch in channels:
        Ts = 1/sig['Fs'][0]
        t = np.arange(0, Ts*len(sig['value'][ch]), Ts)
        fig, axs = plt.subplots(2, figsize=(12,12))
        fig.suptitle('Signal channel ' + str(ch))
        axs[0].plot(t, sig['value'][ch,:])
        axs[0].set_xlabel('Time [s]')
        axs[1].plot(psd.loc[psd['ch']==ch]['f'].values[0], np.log(psd.loc[psd['ch']==ch]['pxx'].values[0]), label='ch '+ str(ch), color='firebrick')
        axs[1].set_title('Power Spectral Density')
        axs[1].set_xscale('log')
        axs[1].set_xlabel('Frequency [Hz]')
        axs[1].set_ylabel('log (pxx)')
        plt.savefig(output_dir + '/channel_' + str(ch))
        plt.close()

parser = argparse.ArgumentParser()
parser.add_argument('signal_file', help='file where the signal is stored (.mat)')
parser.add_argument('psd_file', help='file where the signal PSD is stored (.json)')
parser.add_argument('--output_dir', default='output', help='directory where output plots will be stored')
parser.add_argument('--channels', nargs="+", type=int, metavar='ch', help='selected signal channels')
args = parser.parse_args()

# visualize signal and PSD
visualization(args.signal_file, args.psd_file, args.channels, args.output_dir)
