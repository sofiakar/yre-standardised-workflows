# **"psd_sig_vis"** CommandLineTool

### Description
Plots the signal and the PSD (Power Spectral Density) together, for every selected channel of a signal

### Inputs
- **sig_file**: input file where the signal is stored (.mat)
- **psd_file**: input file where the signal PSD is stored (.json)
- **output_dir_name**: preferred name for output directory
- **channels**: selected signal channels

### Outputs
- **plots**: produced directory containing all relevant plots (signal and PSD for each selected channel)
