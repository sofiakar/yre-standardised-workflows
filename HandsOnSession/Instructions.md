# Setup and VM access

For the purpose of this hands-on session, we have created a Virtual Machine with all the necessary software pre-installed. To access the VM:

    ssh researcherXX@148.187.149.109

All the material is accessible in a [gitlab repo](https://gitlab.ebrains.eu/sofiakar/yre-standardised-workflows) that you need to clone into the VM. In this repo, we have created a folder [CommandLineTools](/CommandLineTools/), which holds EBRAINS tools already defined via CWL syntax (and also the source code and Dockerfiles to get a better understanding). Using [Rabix](#visual-programming-rabix), you can graphically visualise the tools and connect them together in order to create workflows. There is also a folder [Workflows](/Workflows), with predefined workflows using those CommandLineTools, that can be executed as is.

    (on vm) git clone https://gitlab.ebrains.eu/sofiakar/yre-standardised-workflows.git

In order to access the remote files (VM) from your local machine, you can mount the repo directory to an empty folder in your machine.

    (on pc) mkdir ~/Desktop/vm_dir
    (on pc) sudo apt install sshfs
    (on pc) sshfs researcherXX@148.187.149.109:/home/researcherXX/yre-standardised-workflows/ ~/Desktop/vm_dir/

# Tool and Workflow Definition

### Code editors

You can define your command line tools and workflows following [CWL syntax](https://www.commonwl.org/v1.2/).
Most code editors ([vscode](https://github.com/manabuishii/vscode-cwl), [atom](https://github.com/manabuishii/language-cwl), [vim](https://github.com/manabuishii/vim-cwl)) have editing modes or support CWL.

### Visual Programming: Rabix

In this tutorial we will use [Rabix Composer](http://docs.rabix.io/rabix-composer-home), that allows users to experiment with dragging and droping different EBRAINS command line tools for creating new workflows. For Linux OS, Rabix Composer can be downloaded as an AppImage from https://github.com/rabix/composer/releases. You then need to unzip it and make it executable:

    chmod +x rabix-composer-1.5.0-linux.AppImage

    *(sudo apt install fuse-convmvfs in case of FUSE error)

Rabix Composer is not actively maintained as of Q2 2021. **[Rabix Benten](https://github.com/rabix/benten)** is a language server for CWL, that provides CWL code intelligence for VS Code, vim/neovim, Emacs, Acme, IntelliJ/JetBrains, and others, including graphical visualisation.

# Workflow Execution

CWL workflow definitions (workflow.cwl) describe the "recipe" of the scientific work. To execute a workflow, we need to have a YAML file (e.g. workflow_info.yml) with the input values of the workflow. Essentially, CWL workflows are defined like functions, and the input parameters are given in the YAML file.

### On VM

On the provided VM, all necessary software (docker, cwltool, toil) is already installed, so you just need to select a workflow and use a workflow engine (e.g. cwltool or toil) to execute it.

    cd /home/researcherXX/yre-standardised-workflows/Workflows/PSD_workflow_KG_1/

then

    cwltool workflow.cwl workflow_info.yml

or

    toil-cwl-runner workflow.cwl workflow_info.yml

### Installation Instructions for execution anywhere else (local, cluster, HPC)

To execute workflows and tools defined via Common Workflow Language, the following need to be installed:

- A Container Runtime:

    - Docker
    Installation Instructions: https://docs.docker.com/get-docker/

    - Singularity
    Installation Instructions: https://sylabs.io/guides/3.0/user-guide/installation.html

- A Workflow Engine:
    - Toil
        
            pip install virtualenv
            virtualenv ~/venv
            source ~/venv/bin/activate
            pip install toil[cwl]

    - cwltool
        
            pip install virtualenv
            virtualenv ~/venv
            source ~/venv/bin/activate
            pip install cwltool

Note: In case the software is already available where the execution will take place, no container runtime is needed. If the tools already specify a [DockerRequirement](https://www.commonwl.org/v1.2/CommandLineTool.html#DockerRequirement) under hints, 

    --no-container 
    
can be used as argument to the different workflow engines(toil, cwltool) to avoid it.
