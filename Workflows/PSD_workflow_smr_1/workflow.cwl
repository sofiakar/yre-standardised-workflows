#!/usr/bin/env cwltool

cwlVersion: v1.0
class: Workflow

inputs:
  smr_file: File
  channels: int[]
  mat_file_name: string
  psd_output_file_name: string
  output_file_name: string

outputs:
  final_output:
    type: File
    outputSource: visualization/plot

steps:
  input_conversion:
    run: steps/smr_to_mat.cwl
    in:
      smr_file: smr_file
      mat_file_name: mat_file_name
    out: [converted_file]

  psd_calculation:
    run: steps/psd_calc.cwl
    in:
      input_file: input_conversion/converted_file
      output_file_name: psd_output_file_name
      channels: channels
    out: [output_file]

  visualization:
    run: steps/psd_vis.cwl
    in:
      input_file: psd_calculation/output_file
      output_file_name: output_file_name
      channels: channels
    out: [plot]
