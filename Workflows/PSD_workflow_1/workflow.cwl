#!/usr/bin/env cwltool

cwlVersion: v1.0
class: Workflow

inputs:
  input_file: File
  channels: int[]
  psd_output_file_name: string
  output_file_name: string

outputs:
  final_output:
    type: File
    outputSource: visualization/plot

steps:
  psd_calculation:
    run: steps/psd_calc.cwl
    in:
      input_file: input_file
      output_file_name: psd_output_file_name
      channels: channels
    out: [output_file]

  visualization:
    run: steps/psd_vis.cwl
    in:
      input_file: psd_calculation/output_file
      output_file_name: output_file_name
      channels: channels
    out: [plot]
