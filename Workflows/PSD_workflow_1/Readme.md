# **"PSD calculation from .mat input file and visualisation of all channels"** Workflow

This is a CWL [Workflow](workflow.cwl) consisting of [2 steps (CommandLineTools)](steps).

This workflow calculates the PSD (Power Spectral Density) from an input file containing a signal (.mat) and plots the PSD of all selected signal channels in a single diagram.

Pre-defined inputs can be found in the [workflow_info.yaml](workflow_info.yml) file.


## Inputs
-  **input_file**: the name of the .mat input file
-  **channels**: the channels for which the PSD will be calculated and plotted
-  **psd_output_file_name**: preferred name for the intermediate .json file containing the PSD
-  **output_file_name**: preferred name for the plot containing the PSD visualization


## Outputs
- **final_output**: produced plot containing the PSD visualization


## Steps

### Step 1: [PSD Calculation](steps/psd_calc.cwl) 

Calculates the PSD (Power Spectral Density) for every selected channel of the signal.

Tool syntax along with Dockerfile and input yaml under: [CommandLineTools/psd_calc](/CommandLineTools/psd_calc)


### Step 2: [Visualisation](steps/psd_vis.cwl)  

Plots the PSD (Power Spectral Density) for all selected channels of the signal in a single diagram.

Tool syntax along with Dockerfile and input yaml under: [CommandLineTools/psd_vis](/CommandLineTools/psd_vis)
