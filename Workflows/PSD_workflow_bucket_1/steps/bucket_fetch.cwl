#!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: bucket_fetch.py
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/tc/cwl-tools/bucket_fetch:latest
inputs:
  bucket_id:
    type: string
    inputBinding:
      position: 1
  object_name:
    type: string
    inputBinding:
      position: 2
  token:
    type: string
    inputBinding:
      position: 3
outputs:
  fetched_file:
    type: File
    outputBinding:
      glob: $(inputs.object_name)
