#!/usr/bin/env cwltool

cwlVersion: v1.0
class: Workflow

inputs:
  bucket_id: string
  input_file: string
  channels: int[]
  psd_output_file_name: string
  output_file_name: string
  token: string

outputs:
  final_output:
    type: File
    outputSource: visualization/plot

steps:
  data_fetching:
    run: steps/bucket_fetch.cwl
    in:
      bucket_id: bucket_id
      object_name: input_file
      token: token
    out: [fetched_file]

  psd_calculation:
    run: steps/psd_calc.cwl
    in:
      input_file: data_fetching/fetched_file
      output_file_name: psd_output_file_name
      channels: channels
    out: [output_file]

  visualization:
    run: steps/psd_vis.cwl
    in:
      input_file: psd_calculation/output_file
      output_file_name: output_file_name
      channels: channels
    out: [plot]
