# **"PSD calculation from .smr input file and visualisation per channel"** Workflow

This is a CWL [Workflow](workflow.cwl) consisting of [3 steps (CommandLineTools)](steps).

This workflow converts an input .smr file containing a signal to a .mat file, calculates the PSD (Power Spectral Density) and plots the signal and the PSD together, for every selected channel of the signal.

Pre-defined inputs can be found in the [workflow_info.yaml](workflow_info.yml) file.


## Inputs
-  **smr_file**: the name of the .smr input file
-  **channels**: the channels for which the PSD will be calculated and plotted
-  **mat_file_name**: preferred name for the converted .mat file
-  **psd_output_file_name**: preferred name for the intermediate .json file containing the PSD
-  **output_dir_name**: preferred name for the output directory containing the PSD visualizations


## Outputs
- **final_output**: a directory containing the produced PSD visualizations per channel


## Steps

### Step 1: [Data Conversion](steps/smr_to_mat.cwl)

Converts the .smr input file to a .mat file.

Tool syntax along with Dockerfile and input yaml under: [CommandLineTools/smr_to_mat](/CommandLineTools/smr_to_mat)


### Step 2: [PSD Calculation](steps/psd_calc.cwl)

Calculates the PSD (Power Spectral Density) for every selected channel of the signal.

Tool syntax along with Dockerfile and input yaml under: [CommandLineTools/psd_calc](/CommandLineTools/psd_calc)


### Step 3: [Visualisation](steps/psd_sig_vis.cwl)

Plots the signal and the PSD (Power Spectral Density) together, for every selected channel of the signal.

Tool syntax along with Dockerfile and input yaml under: [CommandLineTools/psd_sig_vis](/CommandLineTools/psd_sig_vis)
