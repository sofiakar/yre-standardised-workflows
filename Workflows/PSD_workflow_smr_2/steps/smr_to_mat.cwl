#!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: smr_to_mat.py
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/tc/cwl-tools/smr_to_mat:latest
inputs:
  smr_file:
    type: File
    inputBinding:
      position: 1
  mat_file_name:
    type: string
    inputBinding:
      prefix: --output_file
      position: 2
outputs:
  converted_file:
    type: File
    outputBinding:
      glob: $(inputs.mat_file_name)
