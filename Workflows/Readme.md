# Structure of the folder
This folder contains CWL workflows that were defined by linking different tools located under [/CommandLineTools](/CommandLineTools) in accordance with their inputs / outputs.
