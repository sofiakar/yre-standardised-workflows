# **"PSD calculation with input from Collab bucket and visualisation per channel"** Workflow

This is a CWL [Workflow](workflow.cwl) consisting of [3 steps (CommandLineTools)](steps).

This workflow fetches an input file containing a signal (.mat) from a Collab bucket, calculates the PSD (Power Spectral Density) and plots the signal and the PSD together, for every selected channel of the signal.

Pre-defined inputs can be found in the [workflow_info.yaml](workflow_info.yml) file.


## Inputs
-  **bucket_id**: the id of the bucket containing the input file
-  **input_file**: the name of the .mat input file (located in the selected bucket)
-  **channels**: the channels for which the PSD will be calculated and plotted
-  **psd_output_file_name**: preferred name for the intermediate .json file containing the PSD
-  **output_dir_name**: preferred name for the output directory containing the PSD visualizations
-  **token**: user's token for the data proxy (clb_oauth.get_token())


## Outputs
- **final_output**: a directory containing the produced PSD visualizations per channel


## Steps

### Step 1: [Data Fetching](steps/bucket_fetch.cwl)

Fetches the input file from a Collab bucket using the Data Proxy.

Tool syntax along with Dockerfile and input yaml under: [CommandLineTools/bucket_fetch](/CommandLineTools/bucket_fetch)


### Step 2: [PSD Calculation](steps/psd_calc.cwl)

Calculates the PSD (Power Spectral Density) for every selected channel of the signal.

Tool syntax along with Dockerfile and input yaml under: [CommandLineTools/psd_calc](/CommandLineTools/psd_calc)


### Step 3: [Visualisation](steps/psd_sig_vis.cwl)

Plots the signal and the PSD (Power Spectral Density) together, for every selected channel of the signal.

Tool syntax along with Dockerfile and input yaml under: [CommandLineTools/psd_sig_vis](/CommandLineTools/psd_sig_vis)
