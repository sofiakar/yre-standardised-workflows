#!/usr/bin/env cwltool

cwlVersion: v1.0
class: Workflow

inputs:
  bucket_id: string
  input_file: string
  channels: int[]
  psd_output_file_name: string
  output_dir_name: string
  token: string

outputs:
  final_output:
    type: Directory
    outputSource: visualization/plots

steps:
  data_fetching:
    run: steps/bucket_fetch.cwl
    in:
      bucket_id: bucket_id
      object_name: input_file
      token: token
    out: [fetched_file]

  psd_calculation:
    run: steps/psd_calc.cwl
    in:
      input_file: data_fetching/fetched_file
      output_file_name: psd_output_file_name
      channels: channels
    out: [output_file]

  visualization:
    run: steps/psd_sig_vis.cwl
    in:
      sig_file: data_fetching/fetched_file
      psd_file: psd_calculation/output_file
      output_dir_name: output_dir_name
      channels: channels
    out: [plots]
