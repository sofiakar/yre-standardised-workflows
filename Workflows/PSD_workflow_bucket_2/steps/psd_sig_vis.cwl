#!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: psd_sig_vis.py
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/tc/cwl-tools/psd_sig_vis:latest
inputs:
  sig_file:
    type: File
    inputBinding:
      position: 1
  psd_file:
    type: File
    inputBinding:
      position: 2
  output_dir_name:
    type: string
    inputBinding:
      prefix: --output_dir
      position: 3
  channels:
    type: int[]
    inputBinding:
      prefix: --channels
      position: 4
outputs:
  plots:
    type: Directory
    outputBinding:
      glob: $(inputs.output_dir_name)
