# YRE-Standardised workflows

This session is of interest to Young Researchers who wish to have a more in depth understanding of "Standardized Computational Workflows" as introduced at EBRAINS Research Infrastructure.

[EBRAINS](https://ebrains.eu/) is a digital research infrastructure built by Human Brain Project. EBRAINS started in 2013 and is one of the largest brain research projects in the world. EBRAINS gathers an extensive range of Data, Tools and Services accessible through [EBRAINS Knowledge Graph](https://search.kg.ebrains.eu/?facet_type[0]=Dataset) and at the same time offers a [powerful underlying infrastructure](https://ebrains.eu/service/computing-resources) to its users for submitting, executing and monitoring large computational workflows for their scientific objectives.

Standardization is the link between defining and authoring computational workflows and the diverse underlying infrastructure provided by EBRAINS. Common Workflow Language (CWL) is the standard, open and common way proposed for authoring, executing and monitoring (via compatible with CWL workflow engines) computational workflows at EBRAINS.


## Learning Objectives
In this Hands-on session we will go through the Common Workflow Language (CWL) format. We will understand how CWL tools wrapped inside containers (via Docker) can be linked together to create (via Rabix) graphs, loops or branches (cwlviewer) also known as CWL workflows.

### Docker
Overview of containerization

Overview of basic Docker commands

Pull and Run pre-defined containers via DockerHub or via Harbor (EBRAINS dedicated docker registry)


### Common Workflow Language
Overview of syntax and structure (tool && workflow definitions)

Execute CWL tools and workflows

### Rabix
Familiarize with Rabix composer

Easy drag & drop tools for creating new workflows

## Structure of the repo
Workflows folder represent some pre-defined workflows. They contain:
- one workflow definition CWL file
- one (at least) job description YAML file with the specific inputs/outputs that should be used

Navigate to one of the them by:

    cd Workflows
    cd <subfolder>

### CommandLineTools folder

This folder contains all CWL tools that will be used to create CWL Workflows via Rabix composer

### helloworld_CommandLineTool
This folder containes a basic CWL tool definition

### template_workflow
This folder contains a template that provides the basic file structure to easily create a CWL workflow, without necessarily having in depth background information on CWL descriptions or Dockerfiles. This file structure includes:

one directory for each workflow step (each workflow step corresponds to a command line tool).
A command-line tool is defined as a piece of software that carries out a specific computational task and runs as a non-interactive program. Two files need to be created per command line tool:

a Dockerfile, that contains instructions on how to build the Docker image that wraps the tool and its dependencies
a CWL tool description that describes the semantics, syntax and execution details of the tool


a CWL workflow description that documents all aspects of the workflow, including all its steps, inputs and outputs files (workflow recipe)
one or more .yml files containing the specific inputs used for different executions of the workflow (workflow input parameters to run recipe with)



## Instructions on installling different engines -Out of scope of this Hands-On session

This section holds information related to installing different workflow engines and executing CWL tools and workflows:

## cwltool

#### Installation:

	pip install virtualenv
	virtualenv ~/venv
	source ~/venv/bin/activate
	pip install cwltool

#### Using cwltool (needs Docker):

	cwltool workflow.cwl workflow_info.yml

#### Using cwltool with Singularity (in HPC):

	module load singularity
	cwltool --singularity workflow.cwl workflow_info.yml

## Toil

#### Installation:

	pip install virtualenv
	virtualenv ~/venv
	source ~/venv/bin/activate
	pip install toil[cwl]

#### Using Toil locally (needs Docker):

	toil-cwl-runner workflow.cwl workflow_info.yml

#### Using Toil with Singularity (in HPC):

	module load singularity    
	toil-cwl-runner --singularity workflow.cwl workflow_info.yml


#### Using Toil on SLURM with Singularity (in HPC):

	module load singularity      
	export TOIL_SLURM_ARGS="-A <project> -C <mc|gpu>"    
	mkdir $SCRATCH/tmp    
	export TMPDIR=$SCRATCH/tmp    
	toil-cwl-runner --setEnv PATH=$PATH --disableCaching --batchSystem=slurm --singularity  workflow.cwl workflow_info.yml
	(--logDebug --debugWorker flags for debugging)
